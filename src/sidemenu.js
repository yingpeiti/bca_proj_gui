import React from 'react';
import SideNav, { Nav, NavIcon, NavText } from 'react-sidenav';
import SvgIcon from 'react-icons-kit';
import { Container, Row, Col, Button } from 'reactstrap';
import { menu } from 'react-icons-kit/icomoon/menu';
 
import { ic_aspect_ratio } from 'react-icons-kit/md/ic_aspect_ratio';
import { ic_business } from 'react-icons-kit/md/ic_business';
import { ic_cancel } from 'react-icons-kit/md/ic_cancel';       
import { caretLeft } from 'react-icons-kit/fa/caretLeft';       
import { caretRight } from 'react-icons-kit/fa/caretRight';       



export default class SideMenu extends React.Component {

    constructor(props) {
        super(props);
        this.open=this.open.bind(this)
        this.state = {
          isOpen: true,
        };

        this.main = {
         background: '#2c3e50', color: '#FFF', height: '100%', display: 'block'
        };

        this.main2 = {
         background: '#2c3e50', color: '#FFF', height: '100%', display: 'none'
        };
   
    }

    open(){
        this.setState({isOpen: !this.state.isOpen})
	console.log("open");
    }    

    autoscroll(){
      var textarea = document.getElementById("textarea");

      textarea.onkeyup = function(evt) {
        this.scrollTop = this.scrollHeight;
      }

    }

    render (){
//specify the base color/background of the parent container if needed 
        var curstyle = this.state.isOpen?this.main:this.main2;
        var button = this.state.isOpen?caretLeft:caretRight;

        const navsty={
        float: 'right',
        fontSize: '10pt'
        }

        const navtsty={
        float: 'right',
        margin: '0px',
        padding: '0px',
        fontSize: '10pt'
        }


	return (
            <div>
            <Container size='sm' style={{ margin: '0px', padding: '0px'}} fluid>
              <Row class='body' style={{margin: '0px', padding: '0px'}}>
                <Col size='10' style={{ margin: '0px', padding: '0px'}}>
		      <div style={curstyle}> 
			<SideNav highlightColor='#E91E63' highlightBgColor='#00bcd4' defaultSelected='sales' > 
			    <Nav id='filters'>
				<NavIcon><SvgIcon size={20} icon={ic_aspect_ratio}/></NavIcon>    
				<NavText> Filters </NavText>
			    </Nav>
			    <Nav id='needs1'>
				<NavText style={navtsty}> [needs] Reduce Workflow Length <SvgIcon size={15} icon={ic_cancel} style={{marginLeft: '10px'}}/></NavText>
			    </Nav>

			    <Nav id='proj1'>
				<NavText style={navtsty}>  [project] Wing Statistic Gatherer <SvgIcon size={15} icon={ic_cancel} style={{marginLeft: '10px'}}/></NavText>
			    </Nav>


			    <Nav id='acts1' style={navsty}>
				<NavText style={navtsty}> [action] Proposal Submitted<SvgIcon size={15} icon={ic_cancel} style={{marginLeft: '10px'}}/></NavText>
			    </Nav>




			</SideNav>
		      </div>
                </Col>
		    <div onClick={() => { this.open();}} style={{height: '80px', minWidth: '18px', margin: '0px 10px 0px 0px' , padding: '3px', borderRadius: '0px 4px 4px 0px', backgroundColor:'#7777ff', color: '#ffffff', paddingTop: '30px', textAlign: 'center'}}><SvgIcon size={20} icon={button} /></div>
              </Row>
            </Container>
            </div>
	);

    }


}
