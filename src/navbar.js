import React from 'react';
import Icon from 'react-icons-kit';
import { menu } from 'react-icons-kit/icomoon/menu';       

import { Nav, NavItem, Dropdown, DropdownItem, DropdownToggle, DropdownMenu, NavLink, InputGroup, InputGroupAddon, Input } from 'reactstrap';



export default class MyNav extends React.Component {
  constructor(props) {
    super(props);

    this.toggle = this.toggle.bind(this);
    this.state = {
      dropdownOpen: false
    };
  }

  toggle() {
    this.setState({
      dropdownOpen: !this.state.dropdownOpen
    });
  }

  render() {
    const style = {
        margin: 0,
        top: 10,
        right: 60,
        bottom:20,
        left: 'auto',
        position: 'fixed',
        zIndex: 10
    };

    return (
        <Nav pills style={style}>
          <Dropdown nav size='sm' isOpen={this.state.dropdownOpen} toggle={this.toggle}>
            <DropdownToggle nav caret className='menu'>
            <Icon icon={menu}  style={{marginRight: '5px'}}/>  
            </DropdownToggle>
            <DropdownMenu style={{width: '320px'}} right>
              <DropdownItem style={{margin: '0px', padding: '10px'}} disabled>
                <InputGroup size="sm" style={{margin: '0px', padding: '0px'}}>
                  <Input placeholder="Search" />
                </InputGroup>
              </DropdownItem>
              <DropdownItem>Settings</DropdownItem>
              <DropdownItem divider />
              <DropdownItem>Logout</DropdownItem>
            </DropdownMenu>
          </Dropdown>
        </Nav>
    );
  }
}
