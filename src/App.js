import React, { Component } from 'react';
import MyTabs from './tabs.js'
import MyNav from './navbar.js'
import SideMenu from './sidemenu.js'
import SearchIn from './SearchIn.js'
import MyHead from './headers.js'

import { Container, Row, Col, Jumbotron } from 'reactstrap';
import './App.css';
import '../node_modules/bootstrap/dist/css/bootstrap.css';


class App extends Component {
  render() {
    return (
      <div className="App" style={{height: '100%'}}>
        <Container style={{ margin: '0px', padding: '0px'}} fluid>

{/*----------- top menu ---------------*/}
         <Row style={{margin: '0px'}}>
            <div class="topmenu">
              <MyNav></MyNav>
            </div>
          </Row>


{/*----------- spacer ---------------*/}
          <Row style={{margin: '10px 0px 10px 0px'}}>
         </Row>



          <Row class='body' style={{margin: '0px', padding: '0px'}}>

{/*----------- side menu  ---------------*/}
            <SideMenu></SideMenu>
            <Col size='12' style={{ margin: '0px', padding: '0px'}}>

              <MyHead></MyHead>


{/*----------- Tabs  ---------------*/}
              <MyTabs></MyTabs>
             </Col>
          </Row>
        </Container>
      </div>
    );
  }
}

export default App;
