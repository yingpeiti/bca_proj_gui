import React from 'react';
import DragSortableList from 'react-drag-sortable'
import { Badge, TabContent, TabPane, Nav, NavItem, NavLink, Card, Button, CardTitle, CardText, Row, Col } from 'reactstrap';


export default class MyTabs extends React.Component {
  constructor(props) {
    super(props);

    this.toggle = this.toggle.bind(this);
    this.state = {
      activeTab: '1'
    };
  }

  toggle(tab) {
    if (this.state.activeTab !== tab) {
      this.setState({
        activeTab: tab
      });
    }
  }
  render() {
    var classnames = require('classnames');
   
    const style={
      margin: 0,
      padding: 0,
    };

 var list = [
    {content: (<div><span class="plist">Reduce Workflow length</span><Badge style={{marginLeft: '5px'}}>Project Developement</Badge><Badge style={{marginLeft: '5px'}}>Engine Maintenance Scheduler</Badge></div>)},
    {content: (<div><span class="plist">Decrease Manufacture Defects</span><Badge style={{marginLeft: '5px'}}>Wing Statistic Gatherer</Badge><Badge style={{marginLeft: '5px'}}>Wheel Allocation System</Badge></div>)},
    {content: (<div><span class="plist">Increase Reliability Predictions</span><Badge style={{marginLeft: '5px'}}>Audio Recorder</Badge><Badge style={{marginLeft: '5px'}}>Wing Statistic Gatheer</Badge></div>)}
];




 var plist = [
    {content: (<div><span class="plist">Project Development</span><span class="plist_act"><a href="">edit</a></span><span class="plist_act"><a href="">del</a></span><span class="plist_act"><a href="">filter by</a></span></div>)},
    {content: (<div><span class="plist">Wing Statistic Gatherer</span><span class="plist_act"><a href="">edit</a></span><span class="plist_act"><a href="">del</a></span><span class="plist_act"><a href="">filter by</a></span></div>)},
    {content: (<div><span class="plist">Engine Maintenance Scheduler</span><span class="plist_act"><a href="">edit</a></span><span class="plist_act"><a href="">del</a></span><span class="plist_act"><a href="">filter by</a></span></div>)},
    {content: (<div><span class="plist">Fuselage Check List</span><span class="plist_act"><a href="">edit</a></span><span class="plist_act"><a href="">del</a></span><span class="plist_act"><a href="">filter by</a></span></div>)},
    {content: (<div><span class="plist">Wheel Allocation System</span><span class="plist_act"><a href="">edit</a></span><span class="plist_act"><a href="">del</a></span><span class="plist_act"><a href="">filter by</a></span></div>)},
    {content: (<div><span class="plist">Audio Recorder</span><span class="plist_act"><a href="">edit</a></span><span class="plist_act"><a href="">del</a></span><span class="plist_act"><a href="">filter by</a></span></div>)}
 ];

 var alist = [
    {content: (<div><font color="red" style={{paddingRight: '10px', fontSize: '10pt'}}>Due: Now</font><Badge style={{marginRight: '5px'}}>Project Developement</Badge><span class="plist">Code Review</span><span class="plist_act"><a href="">edit</a></span><span class="plist_act"><a href="">del</a></span><span class="plist_act"><a href="">filter by</a></span></div>)},
    {content: (<div><font color="blue" style={{paddingRight: '10px', fontSize: '10pt'}}>Due: 2017-12-15 14:00:00 PDT</font><Badge style={{marginRight: '5px'}}>Wing Statistic Gatherer</Badge><span class="plist">Submit Proposal</span><span class="plist_act"><a href="">edit</a></span><span class="plist_act"><a href="">del</a></span><span class="plist_act"><a href="">filter by</a></span></div>)},
    {content: (<div><font color="blue" style={{paddingRight: '10px', fontSize: '10pt'}}>Due: 2018-01-10 09:00:00 PDT</font><Badge style={{marginRight: '5px'}}>Engine Maintenance Scheduler</Badge><span class="plist">Final Approval</span><span class="plist_act"><a href="">edit</a></span><span class="plist_act"><a href="">del</a></span><span class="plist_act"><a href="">filter by</a></span></div>)},
    {content: (<div><font color="blue" style={{paddingRight: '10px', fontSize: '10pt'}}>Due: 2018-01-29 11:00:00 PDT</font><Badge style={{marginRight: '5px'}}>Fuselage Check List</Badge><span class="plist">Fuselage Check List</span><span class="plist_act"><a href="">edit</a></span><span class="plist_act"><a href="">del</a></span><span class="plist_act"><a href="">filter by</a></span></div>)},
    {content: (<div><font color="blue" style={{paddingRight: '10px', fontSize: '10pt'}}>Due: 2018-02-02 15:00:00 PDT</font><Badge style={{marginRight: '5px'}}>Wheel Allocation System</Badge><span class="plist">Infrastructure Planning</span><span class="plist_act"><a href="">edit</a></span><span class="plist_act"><a href="">del</a></span><span class="plist_act"><a href="">filter by</a></span></div>)},
    {content: (<div><font color="blue" style={{paddingRight: '10px', fontSize: '10pt'}}>Due: 2018-02-11 10:00:00 PDT</font><Badge style={{marginRight: '5px'}}>Audio Recorder</Badge><span class="plist">Concept Development </span><span class="plist_act"><a href="">edit</a></span><span class="plist_act"><a href="">del</a></span><span class="plist_act"><a href="">filter by</a></span></div>)}
 ];


 var mlist = [
    {content: (<div><font color="blue" style={{paddingRight: '10px', fontSize: '10pt'}}>Fulfilled</font><Badge style={{marginRight: '5px'}}>Project Developement</Badge><span class="plist">Minimal Cost</span><span class="plist_act"><a href="">edit</a></span><span class="plist_act"><a href="">del</a></span><span class="plist_act"><a href="">filter by</a></span></div>)},
    {content: (<div><font color="blue" style={{paddingRight: '10px', fontSize: '10pt'}}>Fulfilled</font><Badge style={{marginRight: '5px'}}>Project Development</Badge><span class="plist">Reduce Maintenance Cost</span><span class="plist_act"><a href="">edit</a></span><span class="plist_act"><a href="">del</a></span><span class="plist_act"><a href="">filter by</a></span></div>)},
    {content: (<div><font color="red" style={{paddingRight: '10px', fontSize: '10pt'}}>Not Fulfilled</font><Badge style={{marginRight: '5px'}}>Engine Maintenance Scheduler</Badge><span class="plist">Reduce Maintenance Hours</span><span class="plist_act"><a href="">edit</a></span><span class="plist_act"><a href="">del</a></span><span class="plist_act"><a href="">filter by</a></span></div>)},
    {content: (<div><font color="blue" style={{paddingRight: '10px', fontSize: '10pt'}}>Fulfilled</font><Badge style={{marginRight: '5px'}}>Project Development</Badge><span class="plist">Reduce Maintenance Cost</span><span class="plist_act"><a href="">edit</a></span><span class="plist_act"><a href="">del</a></span><span class="plist_act"><a href="">filter by</a></span></div>)}
 ];



 
    return (
      <div>
        <Nav tabs>
          <NavItem>
            <NavLink
              className={classnames({ active: this.state.activeTab === '1' })}
              onClick={() => { this.toggle('1'); }}
            >
              Needs
            </NavLink>
          </NavItem>
          <NavItem>
            <NavLink
              className={classnames({ active: this.state.activeTab === '2' })}
              onClick={() => { this.toggle('2'); }}
            >
              Projects
            </NavLink>
          </NavItem>
          <NavItem>
            <NavLink
              className={classnames({ active: this.state.activeTab === '3' })}
              onClick={() => { this.toggle('3'); }}
            >
              Actions
            </NavLink>
          </NavItem>

          <NavItem>
            <NavLink
              className={classnames({ active: this.state.activeTab === '4' })}
              onClick={() => { this.toggle('4'); }}
            >
              Metrics
            </NavLink>
          </NavItem>


        </Nav>
        <TabContent activeTab={this.state.activeTab}>
          <TabPane tabId="1" style={style}>
            <Row style={style}>
              <Col sm="12" style={{padding: '10px'}}>
                <DragSortableList items={list} dropBackTransitionDuration={0.3} type="vertical"/>
              </Col>
            </Row>
          </TabPane>
          <TabPane tabId="2">
            <Row style={style}>
              <Col sm="12" style={{padding: '10px'}}>
                <DragSortableList items={plist} dropBackTransitionDuration={0.3} type="vertical"/>
              </Col>
            </Row>
          </TabPane>
          <TabPane tabId="3" style={style}>
            <Row style={style}>
              <Col sm="12" style={{padding: '10px'}}>
                <DragSortableList items={alist} dropBackTransitionDuration={0.3} type="vertical"/>
              </Col>
            </Row>
          </TabPane>
          <TabPane tabId="4" style={style}>
            <Row style={style}>
              <Col sm="12" style={{padding: '10px'}}>
                <DragSortableList items={mlist} dropBackTransitionDuration={0.3} type="vertical"/>
              </Col>
            </Row>
          </TabPane>
        </TabContent>
      </div>
    );
  }
}
