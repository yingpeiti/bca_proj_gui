import React from 'react';
import { InputGroup, InputGroupAddon, Input } from 'reactstrap';

const SearchIn = (props) => {
  return (
    <div>
      <InputGroup size="sm">
        <Input placeholder="Search" />
      </InputGroup>
    </div>
  );
};

export default SearchIn;
