import React from 'react';
import Icon from 'react-icons-kit';
import { Container, Row, Col, Jumbotroni, ListGroup, ListGroupItem } from 'reactstrap';
import { expand } from 'react-icons-kit/fa/expand';       
import { compress } from 'react-icons-kit/fa/compress';       



export default class MyHead extends React.Component {

  constructor(props) {
    super(props);

    this.toggle = this.toggle.bind(this);
    this.state = {
      isOpen: true
    };

   this.style= {
     margin: '0px',
   };
 
   this.style2= {
     margin: '0px',
     display: 'none'
   };
 

  }


  toggle() {

    this.setState({
      isOpen: !this.state.isOpen
    });

  }


 render() {
 let chng;
 let icon;
 let hide={};
 let mainbgc={};
 let bgc={};
 let showbgc={};
  if(this.state.isOpen){
  chng=this.style;
  mainbgc={
   margin: '0px 0px 10px 0px',
   padding: '10px',
   backgroundColor: '#cccccc',
   width: '100%',
   overflow: 'hidden'
  }
  bgc={
  backgroundColor: '#cccccc'
  }
  showbgc={
  backgroundColor: 'transparent',
  padding: '5px'
  }
  icon=compress;
    hide={
    margin: '0px'
    }
  }
  else{
    chng=this.style2;
    icon=expand;
    mainbgc={
      margin: '0px 0px 0px 0px',
      padding: '10px',
      backgroundColor: 'transparent',
      width: '100%',
      overflow: 'hidden'
    }
    showbgc={
    backgroundColor: '#cccccc',
    padding: '5px',
    borderRadius: '3px'
    }
    bgc={
    backgroundColor: 'transparent',
    }
    hide={
    margin: '0px',
    display: 'none'
    }

  }
   return (
      <Container style={mainbgc} fluid>
	<Row>
	<Col lg='11'>
        </Col>
	<Col lg='1' style={{ textAlign: 'right'}} right> 
          <Icon size={20} icon={icon} style={showbgc} onClick={() => { this.toggle(); }} />
        </Col>
        </Row>
	<Row style={hide}>
	  <Col size='4'>
	  <div class="todo" style={{border: '1px solid #ffffff', borderRadius: '3px', backgroundColor: '#ffffff', textAlign: 'left', padding: '8px', height: '150px', marginBottom: '8px', overflow: 'auto' }}>
          To Do List:
	      <ListGroup>
		<ListGroupItem className="justify-content-between">(Project Developement) Code review Overdue.</ListGroupItem>
		<ListGroupItem className="justify-content-between">(Wing Statistic Gatherer) Missing Proposal</ListGroupItem>
		<ListGroupItem className="justify-content-between">(Audio Recorder Revamp) Approvals missing</ListGroupItem>
	      </ListGroup>
          </div>
	  </Col>

	  <Col size='4'>
	  <div class="todo" style={{border: '1px solid #ffffff', borderRadius: '3px', backgroundColor: '#ffffff', textAlign: 'left', padding: '8px', height: '150px', marginBottom: '8px', overflow: 'auto' }}>
          Recent Activities:
	      <ListGroup>

		<ListGroupItem className="justify-content-between">(Wing Statistic Gatherer) Project Submitted</ListGroupItem>
		<ListGroupItem className="justify-content-between">(Engine Maintence Scheduler) Code Reviewed</ListGroupItem>
		<ListGroupItem className="justify-content-between">(Project Developement) Code Submitted</ListGroupItem>

	      </ListGroup>
          </div>


	  </Col>
	</Row>
      </Container>
   );
 }
}
